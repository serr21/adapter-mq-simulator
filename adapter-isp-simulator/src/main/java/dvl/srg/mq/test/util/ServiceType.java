package dvl.srg.mq.test.util;

public enum ServiceType {
    INBOUND, OUTBOUND
}
