package dvl.srg.mq.test.service;

import dvl.srg.mq.mqconnection.QueueConnection;
import dvl.srg.mq.test.util.ServiceType;

/**
 * Created by smotpan on 6/1/2016.
 */
public class ParticipantQueueServiceFactory {

    public static ParticipantQueueService createNewParticipantService(final ServiceType type,
            final String participantID, final QueueConnection<String> queueConnectionFactory) {

        switch (type) {
        case INBOUND:
            return new IPSInboundQueueService(participantID, queueConnectionFactory);
        case OUTBOUND:
            return null;
        default:
            return null;
        }
    }
}
