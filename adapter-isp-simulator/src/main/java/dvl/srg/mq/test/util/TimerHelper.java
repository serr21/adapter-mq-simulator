package dvl.srg.mq.test.util;

/**
 * Created by smotpan on 6/1/2016.
 */
public class TimerHelper {

    public static void sleep(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
