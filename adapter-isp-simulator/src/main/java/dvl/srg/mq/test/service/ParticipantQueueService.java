package dvl.srg.mq.test.service;

/**
 * Created by smotpan on 6/1/2016.
 */
public interface ParticipantQueueService extends Runnable {
    
    public void start();

    public void stop();
}
