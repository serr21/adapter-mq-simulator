package dvl.srg.mq.test;

import dvl.srg.mq.common.participant.Participant;
import dvl.srg.mq.mqconnection.QueueConnection;
import dvl.srg.mq.mqconnection.QueueConnectionFactory;
import dvl.srg.mq.test.configuration.ConfigurationManager;
import dvl.srg.mq.test.configuration.MQConfiguration;
import dvl.srg.mq.test.configuration.QueueManagerConfiguration;
import dvl.srg.mq.test.service.ParticipantQueueServiceFactory;
import dvl.srg.mq.test.util.ServiceType;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import javax.jms.JMSException;


public final class IPSServer {

    private static final String INBOUND_REQUEST_QUEUE = "_INBOUND_REQUEST_QUEUE";
    private static final String INBOUND_RESPONSE_QUEUE = "_INBOUND_RESPONSE_QUEUE";
    private static final String OUTBOUND_REQUEST_QUEUE = "_OUTBOUND_REQUEST_QUEUE";
    private static final String OUTBOUND_RESPONSE_QUEUE = "_OUTBOUND_RESPONSE_QUEUE";

    private static final Logger LOG = Logger.getLogger(IPSServer.class.getName());

    public static void main(final String[] args) throws JMSException {
        LOG.info("IPS Emulator Started...");

        final Participant[] participants = ConfigurationManager.getInstance().getParticipants();

        final ExecutorService service = Executors.newFixedThreadPool(participants.length);

        final MQConfiguration mqConfiguration = new QueueManagerConfiguration().load();

        final QueueConnection<String> queueConnectionFactory = QueueConnectionFactory
                .getConnectionFactory(mqConfiguration);

        for (final Participant participant : participants) {
            final String participantID = getbankQueueID(participant.getId());
            queueConnectionFactory.register(participantID, getParticipantQueueName(participantID));
            service.submit(ParticipantQueueServiceFactory.createNewParticipantService(ServiceType.INBOUND,
                    participantID, queueConnectionFactory));
        }
    }

    private static List<String> getParticipantQueueName(final String participantID) {
        return Arrays.asList(participantID.toUpperCase() + INBOUND_REQUEST_QUEUE,
                participantID.toUpperCase() + INBOUND_RESPONSE_QUEUE,
                participantID.toUpperCase() + OUTBOUND_REQUEST_QUEUE,
                participantID.toUpperCase() + OUTBOUND_RESPONSE_QUEUE);
    }

    private static String getbankQueueID(final String id) {
        return new String(id.substring(0, 4) + id.charAt(id.length() - 1));
    }
}
