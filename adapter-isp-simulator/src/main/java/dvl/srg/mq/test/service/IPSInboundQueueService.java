package dvl.srg.mq.test.service;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;


import dvl.srg.mq.message.MessageFactory;
import dvl.srg.mq.message.MessageQueueSession;
import dvl.srg.mq.mqconnection.QueueConnection;
import dvl.srg.mq.test.util.TimerHelper;

public class IPSInboundQueueService implements ParticipantQueueService {

    private static final int DELAY = 10;
    private static final Logger LOG = Logger.getLogger(IPSInboundQueueService.class.getName());
    private static final String INBOUND_REQUEST_QUEUE = "_INBOUND_REQUEST_QUEUE";
    private static final String INBOUND_RESPONSE_QUEUE = "_INBOUND_RESPONSE_QUEUE";

    private final AtomicBoolean isRun;
    private final String participantID;
    private final MessageQueueSession messageQueueSession;

    public IPSInboundQueueService(final String participantID, final QueueConnection<String> queueConnectionFactory) {
        this.participantID = participantID;
        messageQueueSession = queueConnectionFactory.createMessageQueueSession(participantID);
        isRun = new AtomicBoolean(Boolean.TRUE);
    }

    @Override
    public void start() {

        LOG.info("Start Inbound Service for  " + participantID);
        messageQueueSession.receiveAsync(participantID.toUpperCase() + INBOUND_REQUEST_QUEUE, message -> {
            TimerHelper.sleep(DELAY);
            messageQueueSession.sendMessage(
                    MessageFactory.createMessage(message.getId(), participantID, message.getContent()),
                    participantID.toUpperCase() + INBOUND_RESPONSE_QUEUE);
        });

    }

    @Override
    public void stop() {
        isRun.getAndSet(Boolean.FALSE);
        LOG.info("Stop Inbound Service for  " + participantID);
    }

    @Override
    public void run() {
        start();
    }
}
