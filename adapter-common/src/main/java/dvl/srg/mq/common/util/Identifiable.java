package dvl.srg.mq.common.util;

public interface Identifiable<T> {
	T getId();
}
