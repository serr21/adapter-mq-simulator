package dvl.srg.mq.common.util;

public interface ObjectProvider<T> {
	T get();
}
