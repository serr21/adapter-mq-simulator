package dvl.srg.mq.common.util;

public abstract class LazyObjectProvider<T> implements ObjectProvider<T> {

	private T obj;

	private volatile boolean load = true;

	@Override
	public final T get() {
		if (load) {
			init();
		}
		return obj;
	}

	private synchronized void init() {
		if (load) {
			obj = load();
			load = false;
		}
	}

	protected abstract T load();

}
