package dvl.srg.mq.common.util;

public interface InstanceBuilder<T> {
	T build();
}
