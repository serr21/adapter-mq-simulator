package dvl.srg.mq.common.util;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public interface IOUtils {

	public static void closeQuietly(final AutoCloseable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (final Exception e) {
				Logger.getLogger(IOUtils.class.getName()).log(Level.FINEST, "Close opperation failed...", e);
			}
		}
	}

	public static int transfer(final ByteBuffer from, final ByteBuffer to) {
		final int len = Math.min(from.remaining(), to.remaining());
		for (int i = 0; i < len; i++) {
			to.put(from.get());
		}
		return len;
	}

	public static byte[] getBytes(final ByteBuffer from) {
		final byte[] ret = new byte[from.remaining()];
		from.get(ret);
		return ret;
	}

	public static byte[] getBytes(final ByteBuffer from, final int newLen) {
		final int len = Math.min(from.remaining(), newLen);
		final byte[] ret = new byte[len];
		from.get(ret);
		return ret;
	}

}
