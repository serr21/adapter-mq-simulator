package dvl.srg.mq.common.participant;

public interface Participant {
	String getId();

	ParticipantConfig getConfiguration();
}
