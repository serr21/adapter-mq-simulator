package dvl.srg.mq.common.message;

public interface Message {
	String getId();

	String getParticipantId();

	String getContent();
}
