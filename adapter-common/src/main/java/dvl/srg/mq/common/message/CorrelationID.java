package dvl.srg.mq.common.message;

import java.util.UUID;

/**
 * Created by smotpan on 6/6/2016.
 */
public final class CorrelationID {

    public final UUID correlationID;

    public CorrelationID(final UUID correlationID) {
        this.correlationID = correlationID;
    }

    public UUID getCorrelationID() {
        return correlationID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CorrelationID that = (CorrelationID) o;

        return correlationID != null ? correlationID.equals(that.correlationID) : that.correlationID == null;

    }

    @Override
    public int hashCode() {
        return correlationID != null ? correlationID.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "CorrenlationID{" + "correlationID=" + correlationID + '}';
    }
}
