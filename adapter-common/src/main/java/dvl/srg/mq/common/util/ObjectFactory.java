package dvl.srg.mq.common.util;

@FunctionalInterface
public interface ObjectFactory<T> {

	static <T> T create(final ObjectFactory<T> factory) {
		return factory == null ? null : factory.create();
	}

	T create();

}
