package dvl.srg.mq.common.participant;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class ParticipantConfig {

	private int[] ports;
	private int socketTimeout = 5;
	private long queueTimeout = 5000;
	private final Participant participant;

	private final Map<Integer, PortType> allPorts = new HashMap<>();

	public ParticipantConfig(final Participant participant) {
		this.participant = participant;
	}

	public int[] getPorts() {
		return ports;
	}

	public Map<Integer, PortType> getAllPorts() {
		return Collections.unmodifiableMap(allPorts);
	}

	public void addPort(final int port, final PortType type) {
		allPorts.put(port, type);
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(final int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public long getQueueTimeout() {
		return queueTimeout;
	}

	public void setQueueTimeout(final long queueTimeout) {
		this.queueTimeout = queueTimeout;
	}

	public Participant getParticipant() {
		return participant;
	}

}
