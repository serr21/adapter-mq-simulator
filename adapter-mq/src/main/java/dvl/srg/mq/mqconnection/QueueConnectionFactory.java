package dvl.srg.mq.mqconnection;


import dvl.srg.mq.test.configuration.MQConfiguration;

public final class QueueConnectionFactory {

    private QueueConnectionFactory() {
    }

    public static QueueConnection<String> getConnectionFactory(final MQConfiguration mqConfiguration) {
        final MQQueueConnectionFactory mqQueueConnectionFactory = new IBMMQQueueConnectionFactory(mqConfiguration);
        return new MQQueueConnection<>(mqQueueConnectionFactory);
    }
}
