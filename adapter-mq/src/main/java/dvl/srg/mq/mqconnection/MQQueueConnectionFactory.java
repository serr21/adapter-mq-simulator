package dvl.srg.mq.mqconnection;

import javax.jms.JMSException;
import javax.jms.QueueConnection;

/**
 * Created by smotpan on 6/8/2016.
 */
public interface MQQueueConnectionFactory {

    public QueueConnection createQueueConnection() throws JMSException;

    public QueueConnection createQueueConnection(final String username, final String password) throws JMSException;

}
