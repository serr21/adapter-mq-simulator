package dvl.srg.mq.mqconnection;

import dvl.srg.mq.message.MessageQueueSession;
import dvl.srg.mq.message.MessageQueueSessionTask;
import dvl.srg.mq.queue.Queue;

import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.JMSException;


public final class MQQueueConnection<T> implements QueueConnection<T> {

    private final Map<T, Map<String, Queue>> queueMap;
    private final MQQueueConnectionFactory mqQueueConnectionFactory;

    public MQQueueConnection(final MQQueueConnectionFactory mqQueueConnectionFactory) {
        this.mqQueueConnectionFactory = mqQueueConnectionFactory;
        this.queueMap = new ConcurrentHashMap<>();
    }

    @Override
    public void register(final T queueIdentifier, final List<String> queues) {
        final Map<String, Queue> queueIdentifiedMap = createQueueIdentifiedMap(requireNonNull(queues));
        queueMap.put(requireNonNull(queueIdentifier), queueIdentifiedMap);
    }

    @Override
    public void unregister(final T queueIdentifier) {
        queueMap.remove(requireNonNull(queueIdentifier));
    }

    @Override
    public MessageQueueSession createMessageQueueSession(final T queueIdentifier) {
        return new MessageQueueSessionTask(mqQueueConnectionFactory, queueMap.get(requireNonNull(queueIdentifier)));
    }

    private Map<String, Queue> createQueueIdentifiedMap(final List<String> queues) {
        final Map<String, Queue> queueIdentifiedMap = new ConcurrentHashMap<>(queues.size());

        queues.parallelStream().unordered().forEach(e -> {
            try {
                queueIdentifiedMap.put(e, new Queue.Builder().setName(e).build());
            } catch (final JMSException e1) {
                e1.printStackTrace();
            }
        });
        return queueIdentifiedMap;
    }
}
