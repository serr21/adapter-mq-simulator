package dvl.srg.mq.mqconnection;

import com.ibm.msg.client.wmq.common.CommonConstants;
import dvl.srg.mq.test.configuration.MQConfiguration;


import javax.jms.JMSException;
import javax.jms.QueueConnection;

import static java.util.Objects.requireNonNull;

/**
 * Created by smotpan on 6/8/2016.
 */
public final class IBMMQQueueConnectionFactory implements MQQueueConnectionFactory {

    private final com.ibm.mq.jms.MQQueueConnectionFactory ibmMQQueueConnectionFactory;
    private final MQConfiguration mqConfiguration;

    public IBMMQQueueConnectionFactory(final MQConfiguration mqConfiguration) {

        this.mqConfiguration = requireNonNull(mqConfiguration);
        ibmMQQueueConnectionFactory = new com.ibm.mq.jms.MQQueueConnectionFactory();
        try {
            init();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public final void init() throws JMSException {
        ibmMQQueueConnectionFactory.setHostName(mqConfiguration.getHostName());
        ibmMQQueueConnectionFactory.setPort(mqConfiguration.getPort());
        ibmMQQueueConnectionFactory.setQueueManager(mqConfiguration.getQueueManager());
        ibmMQQueueConnectionFactory.setChannel(mqConfiguration.getChannel());
        ibmMQQueueConnectionFactory.setTransportType(CommonConstants.WMQ_CM_CLIENT);
    }

    @Override
    public QueueConnection createQueueConnection() throws JMSException {
        return ibmMQQueueConnectionFactory.createQueueConnection(mqConfiguration.getUser(),
                mqConfiguration.getPassword());
    }

    @Override
    public QueueConnection createQueueConnection(String username, String password) throws JMSException {
        return ibmMQQueueConnectionFactory.createQueueConnection(requireNonNull(username), requireNonNull(password));
    }
}
