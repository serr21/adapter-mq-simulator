package dvl.srg.mq.mqconnection;

import dvl.srg.mq.message.MessageQueueSession;

import java.util.List;


public interface QueueConnection<T> {

    public void register(final T queueIdentifier, final List<String> queues);

    public void unregister(final T queueIdentifier);

    public MessageQueueSession createMessageQueueSession(final T queueIdentifier);

}
