package dvl.srg.mq.message;

import dvl.srg.mq.common.message.Message;

@FunctionalInterface
public interface CompletionMessageHandler {

    void doComplete(Message message);

}
