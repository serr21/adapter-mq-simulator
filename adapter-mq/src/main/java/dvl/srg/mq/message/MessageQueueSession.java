package dvl.srg.mq.message;


import dvl.srg.mq.common.message.CorrelationID;
import dvl.srg.mq.common.message.Message;

public interface MessageQueueSession {

	/**
	 * send message to queue(specified queue key)
	 *
	 * @param message
	 * @param queue
	 */
	public CorrelationID sendMessage(final Message message, final String queue);

	/**
	 *
	 * set a message listener to queue
	 *
	 * @param queue
	 * @param handler
	 * @return
	 */
	public void receiveAsync(final String queue, final CompletionMessageHandler handler);

}
