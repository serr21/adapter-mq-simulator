package dvl.srg.mq.message;

import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.QueueConnection;


import dvl.srg.mq.common.message.CorrelationID;
import dvl.srg.mq.common.message.Message;
import dvl.srg.mq.queue.Queue;
import dvl.srg.mq.mqconnection.MQQueueConnectionFactory;
import dvl.srg.mq.queue.mqclient.MQQueueReceiver;
import dvl.srg.mq.queue.mqclient.MQQueueSender;

public class MessageQueueSessionTask implements MessageQueueSession {

    private final Map<String, Queue> queueMap;
    private final MQQueueConnectionFactory mqQueueConnectionFactory;

    public MessageQueueSessionTask(final MQQueueConnectionFactory mqQueueConnectionFactory,
            final Map<String, Queue> queueMap) {
        this.queueMap = queueMap;
        this.mqQueueConnectionFactory = mqQueueConnectionFactory;
    }

    @Override
    public CorrelationID sendMessage(final Message message, final String queue) {
        try {
            final Queue participantQueue = queueMap.get(queue);
            sendMessage(message, participantQueue);
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return new CorrelationID(UUID.fromString(message.getId()));
    }

    @Override
    public void receiveAsync(final String queue, final CompletionMessageHandler handler) {
        try {
            final Queue queue1 = queueMap.get(requireNonNull(queue));
            receiveAsync(requireNonNull(queue1), requireNonNull(handler));
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private QueueConnection createNewQueueConnection(final Queue queue) throws JMSException {
        return mqQueueConnectionFactory.createQueueConnection();
    }

    private void sendMessage(final Message message, final Queue queue) throws JMSException {
        new MQQueueSender(createNewQueueConnection(queue), queue).send(message);
    }

    private void receiveAsync(final Queue queue, final CompletionMessageHandler handler) throws JMSException {
        new MQQueueReceiver(createNewQueueConnection(queue), queue, handler).receiveAsync();
    }

}
