package dvl.srg.mq.message;


import dvl.srg.mq.common.message.Message;

/**
 * Created by smotpan on 6/1/2016.
 */
public abstract class MessageFactory {

	public static Message createMessage(final String messageId, final String participantId, final String content) {
		return new Message() {
			@Override
			public String getId() {
				return messageId;
			}

			@Override
			public String getParticipantId() {
				return participantId;
			}

			@Override
			public String getContent() {
				return content;
			}
		};
	}
}
