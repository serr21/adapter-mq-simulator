package dvl.srg.mq.queue.mqclient;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.Session;

import dvl.srg.mq.queue.Queue;

/**
 * Created by smotpan on 6/8/2016.
 */
public abstract class AbstractQueueConnection {

    private final QueueConnection queueConnection;
    private final Queue queue;

    protected AbstractQueueConnection(final QueueConnection queueConnection, final Queue queue) {
        this.queueConnection = queueConnection;
        this.queue = queue;
    }

    protected QueueConnection getQueueConnection() {
        return queueConnection;
    }

    protected Queue getQueue() {
        return queue;
    }

    protected QueueSession getQueueSession() throws JMSException {
        return queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    protected abstract void connect() throws JMSException;

    protected abstract void disconnect() throws JMSException;
}
