package dvl.srg.mq.queue.mqclient;

import static java.util.Objects.requireNonNull;

import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.QueueConnection;
import javax.jms.TextMessage;


import dvl.srg.mq.message.CompletionMessageHandler;
import dvl.srg.mq.message.MessageFactory;
import dvl.srg.mq.queue.Queue;

/**
 * Created by smotpan on 6/8/2016.
 */
public final class MQQueueReceiver extends AbstractQueueConnection implements MessageListener {

    private static final Logger LOG = Logger.getLogger(MQQueueReceiver.class.getName());

    private final CompletionMessageHandler messageHandler;
    private javax.jms.QueueSession session;
    private javax.jms.QueueReceiver queueReceiver;

    public MQQueueReceiver(final QueueConnection queueConnection, final Queue queue,
            final CompletionMessageHandler messageHandler) {
        super(requireNonNull(queueConnection), requireNonNull(queue));
        this.messageHandler = messageHandler;
    }

    @Override
    protected void connect() throws JMSException {
        session = getQueueSession();
        queueReceiver = session.createReceiver(getQueue().getQueue());
        getQueueConnection().start();
    }

    @Override
    protected void disconnect() throws JMSException {
        queueReceiver.close();
        session.close();
        getQueueConnection().stop();
    }

    public void receiveAsync() {
        try {
            connect();
            queueReceiver.setMessageListener(this);
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMessage(Message message) {
        try {
            final TextMessage receivedMessage = (TextMessage) message;
            final String correlationId = receivedMessage.getJMSCorrelationID();
            System.out.println("[INFO] Received corelation ID: " + correlationId);
            LOG.info("[INFO] Received message: " + receivedMessage.getText() + " from queue " + getQueue().getName());

            messageHandler.doComplete(MessageFactory.createMessage(correlationId, null, receivedMessage.getText()));

            // disconnect();

        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
