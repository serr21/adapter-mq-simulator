package dvl.srg.mq.queue.mqclient;

import dvl.srg.mq.common.message.Message;
import dvl.srg.mq.queue.Queue;

import static java.util.Objects.requireNonNull;

import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.QueueConnection;



/**
 * Created by smotpan on 6/8/2016.
 */
public final class MQQueueSender extends AbstractQueueConnection {

    private static final Logger LOG = Logger.getLogger(MQQueueSender.class.getName());

    private javax.jms.QueueSender queueSender;
    private javax.jms.QueueSession session;

    public MQQueueSender(final QueueConnection queueConnection, final Queue queue) {
        super(requireNonNull(queueConnection), requireNonNull(queue));
    }

    @Override
    protected void connect() throws JMSException {
        getQueueConnection().start();
        session = getQueueSession();
        queueSender = session.createSender(getQueue().getQueue());
    }

    @Override
    protected void disconnect() throws JMSException {
        queueSender.close();
        session.close();
        getQueueConnection().stop();
    }

    public void send(final Message message) {
        try {
            connect();
            // create jms message and send it
            final javax.jms.Message messageToSend = session.createTextMessage(message.getContent());
            messageToSend.setJMSCorrelationID(message.getId());
            queueSender.send(messageToSend);

            LOG.info("Sent message: " + message.getContent() + " to queue " + getQueue().getName());

            disconnect();

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
