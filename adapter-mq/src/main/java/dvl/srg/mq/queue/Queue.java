package dvl.srg.mq.queue;

import javax.jms.JMSException;

import com.ibm.mq.jms.MQQueue;
import dvl.srg.mq.common.util.InstanceBuilder;


public interface Queue {

    String getName();

    javax.jms.Queue getQueue();

    final class Builder implements InstanceBuilder<Queue> {

        private final class DefaultQueue implements Queue {

            private String name;
            private javax.jms.Queue mqQueue;

            @Override
            public String getName() {
                return name;
            }

            @Override
            public javax.jms.Queue getQueue() {
                return mqQueue;
            }
        }

        private DefaultQueue defaultQueue;

        public Builder setName(final String name) throws JMSException {
            getDefaultQueue().name = name;
            getDefaultQueue().mqQueue = new MQQueue(name);
            return this;
        }

        @Override
        public Queue build() {
            final Queue ret = getDefaultQueue();
            defaultQueue = null;
            return ret;
        }

        private DefaultQueue getDefaultQueue() {
            if (defaultQueue == null) {
                defaultQueue = new DefaultQueue();
            }
            return defaultQueue;
        }
    }
}
