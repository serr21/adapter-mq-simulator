package dvl.srg.mq.test.configuration;

/**
 * Created by smotpan on 6/9/2016.
 */
public class MQConfiguration {

    private final String hostName;
    private final int port;
    private final String queueManager;
    private final String channel;
    private final String user;
    private final String password;

    public MQConfiguration(final String hostName, final int port, final String queueManager, final String channel,
            final String user, final String password) {
        this.hostName = hostName;
        this.port = port;
        this.queueManager = queueManager;
        this.channel = channel;
        this.user = user;
        this.password = password;
    }

    public String getHostName() {
        return hostName;
    }

    public int getPort() {
        return port;
    }

    public String getQueueManager() {
        return queueManager;
    }

    public String getChannel() {
        return channel;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final MQConfiguration that = (MQConfiguration) o;

        if (port != that.port) {
            return false;
        }
        if (hostName != null ? !hostName.equals(that.hostName) : that.hostName != null) {
            return false;
        }
        if (queueManager != null ? !queueManager.equals(that.queueManager) : that.queueManager != null) {
            return false;
        }
        if (channel != null ? !channel.equals(that.channel) : that.channel != null) {
            return false;
        }
        if (user != null ? !user.equals(that.user) : that.user != null) {
            return false;
        }
        return password != null ? password.equals(that.password) : that.password == null;

    }

    @Override
    public int hashCode() {
        int result = hostName != null ? hostName.hashCode() : 0;
        result = 31 * result + port;
        result = 31 * result + (queueManager != null ? queueManager.hashCode() : 0);
        result = 31 * result + (channel != null ? channel.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
