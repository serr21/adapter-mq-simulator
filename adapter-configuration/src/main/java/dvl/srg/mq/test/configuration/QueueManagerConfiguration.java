package dvl.srg.mq.test.configuration;

import dvl.srg.mq.test.configuration.util.PropertiesLoader;

import java.util.Properties;


public final class QueueManagerConfiguration {

    private final MQConfiguration config;
    private final Properties queueConfiguration;

    public QueueManagerConfiguration() {
        queueConfiguration = PropertiesLoader.load("queueconfig.properties");
        config = new MQConfiguration(getHostName(), getPort(), getQueueManager(), getChannel(), getUserName(),
                getPassword());
    }

    private String getProperty(final String configurationKey) {
        return queueConfiguration.getProperty(configurationKey);
    }

    private static enum GeneralConfigKeys {
        HOSTNAME("hostname"), PORT("port"), QUEUE_MANAGER("queuemanager"), CHANNEL("channel"), USER("user"), PASSWORD(
                "password");

        private String key;

        public String getKey() {
            return key;
        }

        private GeneralConfigKeys(final String key) {
            this.key = key;
        }
    }

    private String getHostName() {
        return getProperty(GeneralConfigKeys.HOSTNAME.getKey());
    }

    private int getPort() {
        return Integer.valueOf(getProperty(GeneralConfigKeys.PORT.getKey()));
    }

    private String getQueueManager() {
        return getProperty(GeneralConfigKeys.QUEUE_MANAGER.getKey());
    }

    private String getChannel() {
        return getProperty(GeneralConfigKeys.CHANNEL.getKey());
    }

    private String getUserName() {
        return getProperty(GeneralConfigKeys.USER.getKey());
    }

    private String getPassword() {
        return getProperty(GeneralConfigKeys.PASSWORD.getKey());
    }

    public MQConfiguration load() {
        return config;
    }

}
