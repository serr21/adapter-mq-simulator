package dvl.srg.mq.test.configuration;

import dvl.srg.mq.common.participant.Participant;
import dvl.srg.mq.common.participant.ParticipantConfig;
import dvl.srg.mq.common.participant.PortType;
import dvl.srg.mq.common.util.LazyObjectProvider;
import dvl.srg.mq.common.util.ObjectProvider;
import dvl.srg.mq.test.configuration.util.PropertiesLoader;

import java.util.Properties;



public final class ConfigurationManager {
    public static final int SIZE_OF_MESSAGE = 1024;
    public static final int NUM_OF_PORTS_PER_PARTICIPANT = 5;

    private final Participant[] participants;

    private final Properties queueConfiguration;
    private static final ObjectProvider<ConfigurationManager> INSTANCE_PROVIDER = new ConfigurationManagerProvider();

    public static ConfigurationManager getInstance() {
        return INSTANCE_PROVIDER.get();
    }

    private ConfigurationManager() {
        queueConfiguration = PropertiesLoader.load("queueconfig.properties");
        participants = onboardParticipants();
    }

    private static final class ConfigurationManagerProvider extends LazyObjectProvider<ConfigurationManager> {
        @Override
        protected ConfigurationManager load() {
            return new ConfigurationManager();
        }
    }

    public static Properties getQueueConfiguration() {
        return getInstance().queueConfiguration;
    }

    private String getProperty(final String configurationKey) {
        return queueConfiguration.getProperty(configurationKey);
    }

    private static enum GeneralConfigKeys {
        HOSTNAME("hostname"), PORT("port"), QUEUE_MANAGER("queuemanager"), CHANNEL("channel"), USER("user"), PASSWORD(
                "password");

        private String key;

        public String getKey() {
            return key;
        }

        private GeneralConfigKeys(final String key) {
            this.key = key;
        }
    }

    public static String getHostName() {
        return getInstance().getProperty(GeneralConfigKeys.HOSTNAME.getKey());
    }

    public static int getPort() {
        return Integer.valueOf(getInstance().getProperty(GeneralConfigKeys.PORT.getKey()));
    }

    public static String getQueueManager() {
        return getInstance().getProperty(GeneralConfigKeys.QUEUE_MANAGER.getKey());
    }

    public static String getChannel() {
        return getInstance().getProperty(GeneralConfigKeys.CHANNEL.getKey());
    }

    public static String getUserName() {
        return getInstance().getProperty(GeneralConfigKeys.USER.getKey());
    }

    public static String getPassword() {
        return getInstance().getProperty(GeneralConfigKeys.PASSWORD.getKey());
    }

    public Participant[] onboardParticipants() {
        return new Participant[] { new Participant() {
            private final ParticipantConfig conf = new ParticipantConfig(this);

            {
                final int[] ports = new int[NUM_OF_PORTS_PER_PARTICIPANT];
                for (int i = 0; i < ports.length; i++) {
                    conf.addPort(7000 + i, PortType.IN);
                    conf.addPort(7000 + NUM_OF_PORTS_PER_PARTICIPANT + i, PortType.OUT);
                }

                conf.setSocketTimeout(5);
            }

            @Override
            public ParticipantConfig getConfiguration() {
                return conf;
            }

            @Override
            public String getId() {
                return "BANK001";
            }

            @Override
            public int hashCode() {
                return 31 * getId().hashCode();
            }

            @Override
            public boolean equals(final Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }

                final Participant other = (Participant) obj;
                return other.getId().equals(getId());
            }

            @Override
            public String toString() {
                return "Participant[id=" + getId() + ", config.ports=" + conf.getAllPorts() + ", config.socketTimeout="
                        + conf.getSocketTimeout() + ", config.queueTimeout=" + conf.getQueueTimeout() + "]";
            }

        }, new Participant() {
            private final ParticipantConfig conf = new ParticipantConfig(this);

            {
                final int[] ports = new int[NUM_OF_PORTS_PER_PARTICIPANT];
                for (int i = 0; i < ports.length; i++) {
                    conf.addPort(7100 + i, PortType.IN);
                    conf.addPort(7100 + NUM_OF_PORTS_PER_PARTICIPANT + i, PortType.OUT);
                }

                conf.setSocketTimeout(5);
            }

            @Override
            public ParticipantConfig getConfiguration() {
                return conf;
            }

            @Override
            public String getId() {
                return "BANK002";
            }

            @Override
            public int hashCode() {
                return 31 * getId().hashCode();
            }

            @Override
            public boolean equals(final Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }

                final Participant other = (Participant) obj;
                return other.getId().equals(getId());
            }

            @Override
            public String toString() {
                return "Participant[id=" + getId() + ", config.ports=" + conf.getAllPorts() + ", config.socketTimeout="
                        + conf.getSocketTimeout() + ", config.queueTimeout=" + conf.getQueueTimeout() + "]";
            }
        } };

    }

    public Participant[] getParticipants() {
        return participants;
    }
}
