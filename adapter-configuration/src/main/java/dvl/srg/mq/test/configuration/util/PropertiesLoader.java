package dvl.srg.mq.test.configuration.util;

import java.io.InputStream;
import java.util.Properties;

public final class PropertiesLoader {

	public static Properties load(final String fileName) {
		final Properties properties = new Properties();
		try (final InputStream fis = PropertiesLoader.class.getClassLoader().getResourceAsStream(fileName)) {
			properties.load(fis);
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
		return properties;
	}

}
